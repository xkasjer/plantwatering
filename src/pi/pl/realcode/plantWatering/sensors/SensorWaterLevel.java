package pi.pl.realcode.plantWatering.sensors;

public class SensorWaterLevel {
    SensorAdapterInterface adapter;

    public SensorWaterLevel(SensorAdapterInterface adapter) {
        this.adapter = adapter;
    }

    public boolean isEmpty() {

        return adapter.isHigh();
    }
}
