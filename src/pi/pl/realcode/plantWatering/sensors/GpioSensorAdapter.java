package pi.pl.realcode.plantWatering.sensors;

import com.pi4j.io.gpio.*;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GpioSensorAdapter implements SensorAdapterInterface {
    private GpioPinDigitalInput pin;
    private Logger logger;

    public GpioSensorAdapter(Pin pin) {
        init(pin);
    }

    public GpioSensorAdapter() {
        init(RaspiPin.GPIO_28);
    }

    private void init(Pin pin) {
        GpioController gpioController = GpioFactory.getInstance();
        this.pin = gpioController.provisionDigitalInputPin(pin);

        logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {
            fh = new FileHandler("MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isHigh() {

        return pin.isHigh();
    }

    @Override
    public boolean isLow() {
        return pin.isLow();
    }
}
