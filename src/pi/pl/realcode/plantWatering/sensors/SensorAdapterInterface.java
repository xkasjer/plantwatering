package pi.pl.realcode.plantWatering.sensors;

public interface SensorAdapterInterface {

    boolean isHigh();
    boolean isLow();
}
