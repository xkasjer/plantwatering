package pi.pl.realcode.plantWatering.sensors;

public class SensorWaterLevelFactory {

    public SensorWaterLevel build(){
        SensorAdapterInterface adapter = new GpioSensorAdapter();
        return new SensorWaterLevel(adapter);
    }
}
