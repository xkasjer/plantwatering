package pi.pl.realcode.plantWatering;

public interface PumpInterface {

    void start();
    void stop();
    boolean isOn();
}
