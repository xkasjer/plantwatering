package pi.pl.realcode.plantWatering;


public class PumpGpioFactory {
    public PumpInterface build() {
        PumpAdapterInterface pumpAdapter = new GpioPumpAdapter();

        return new Pump(pumpAdapter);
    }
}
