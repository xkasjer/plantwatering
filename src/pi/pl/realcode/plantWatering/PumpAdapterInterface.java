package pi.pl.realcode.plantWatering;

public interface PumpAdapterInterface {
    void on();
    void off();
}
