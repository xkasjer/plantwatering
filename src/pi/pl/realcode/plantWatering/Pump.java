package pi.pl.realcode.plantWatering;


public class Pump implements PumpInterface {

    boolean state = false;
    PumpAdapterInterface pumpAdapter;

    public Pump(PumpAdapterInterface pumpAdapter) {
        this.pumpAdapter = pumpAdapter;
    }

    @Override
    public void start() {
        pumpAdapter.on();
        state = true;
    }

    @Override
    public void stop() {
        pumpAdapter.off();
        state = false;

    }

    public boolean isOn(){
        return state;
    }
}
