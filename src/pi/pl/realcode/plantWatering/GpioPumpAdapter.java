package pi.pl.realcode.plantWatering;

import com.pi4j.io.gpio.*;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GpioPumpAdapter implements PumpAdapterInterface {

    private GpioPinDigitalOutput pumpPin;
    private Logger logger;

    public GpioPumpAdapter(Pin pin) {
        init(pin);
    }

    public GpioPumpAdapter() {
        init(RaspiPin.GPIO_29);
    }

    private void init(Pin pin) {
        GpioController gpioController = GpioFactory.getInstance();
        this.pumpPin = gpioController.provisionDigitalOutputPin(pin);

        this.logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {
            fh = new FileHandler("MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void on() {
        pumpPin.high();
        logger.info("Pump start");

    }

    @Override
    public void off() {
        pumpPin.low();
        logger.info("Pump stop");
    }
}
