package pi;

import pi.pl.realcode.plantWatering.PumpGpioFactory;
import pi.pl.realcode.plantWatering.PumpInterface;
import pi.pl.realcode.plantWatering.sensors.SensorWaterLevel;
import pi.pl.realcode.plantWatering.sensors.SensorWaterLevelFactory;

import java.sql.Timestamp;

public class Main {

    public static void main(String... args) {

        int minutes = 1;
        PumpGpioFactory pumpFactory = new PumpGpioFactory();
        PumpInterface pump = pumpFactory.build();
        SensorWaterLevelFactory waterLevelFactory = new SensorWaterLevelFactory();
        SensorWaterLevel sensor = waterLevelFactory.build();


        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long workTo = timestamp.getTime() + (1000 * 60 * minutes);
        initStart(pump, sensor);
        while (true) {
            if (sensor.isEmpty()) {
                toStop(pump,sensor);

            } else{
                initStart(pump,sensor);
            }
            try {
                Thread.sleep(1000);                 //1000 milliseconds is one second.
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }



    }

    static void initStart(PumpInterface pump, SensorWaterLevel sensor) {
        if (!sensor.isEmpty() && !pump.isOn()) {
            pump.start();
        }
    }
    static void toStop(PumpInterface pump, SensorWaterLevel sensor) {
        if (sensor.isEmpty() && pump.isOn()) {
            pump.stop();
        }
    }
}
